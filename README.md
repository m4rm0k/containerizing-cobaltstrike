During Red Team Exercises or Engagements there are cases where operators would like to have multiple cobaltstrike egress listeners on the same VM that teamserver is running.

Also there are cases like a red team exercise where, instead of using other Virtual Machines as proxy IPs to a cobaltstrike's teamserver, an operator could proxy traffic by using alias IP on the same VM that hosts cobaltstrike's teamserver.

Due to the fact that cobaltstrike's teamserver binds to all interfaces/IPs on the Host machine where it runs, the only way to get multiple egress listeners is by running mutliple instances of cobaltstrike on different Hosts.

This is an attempt to automate, via a bash script, the deployment of multiple cobaltstrike teamservers in docker containers on one Virtual Machine acting as Host.

Each teamserver binds to each container's internal IP leaving Host Machine's "external" interfaces unbound.

It is up to the operator to bind, during the setup phase, specific Host's "external" interfaces and ports to containers' internal IPs.

Each cobaltstrike instance is running independently using different malleable c2 profile if needed.

Data and logs of each teamserver are stored in different folders on Host machine. So even if a container is stopped or deleted its teamserver's data are kept untouched in the host machine.

Running multiple containers on a Host machine might cause performance issues especially when containers are running Java.

For this reason docker containers are built based on a docker image of a minimal linux distribution (Alpine) with slim modular Java 11 preinstalled.

The provided bash script in this repo allows for an easy and automated installation of such a dockerized  environment.

The purpose of the tests presented in the wiki is to verify that cobaltstrike's functionality within docker containers remains the same as if it was installed on a standalone host VM.

From the operator's point of view having 4 Teamservers running in 4 docker containers on the same Host VM Machine should be the same as having 4 Teamservers in 4 different VM Host Machines.

The assumption in this proposed setup is that multiple IPs are available (physical interfaces or aliases on a single physical one) on a Host VM where docker containers will be deployed. This setup has been tested on Kali and Debian linux distribution with 4GB RAM RAM and 2 virtual Core processors.

The added value of the proposed setup is that a cobaltstrike's operator can have different type of egress Listeners (IPv4 http, IPv4 https, IPv6 http, IPv6 Https) on the same Host VM bound on different IPs and on ports 80 and 443. At the same time the operator can make use of other available IPs on the Host Machine to deploy different C2 Servers like metasploit, covenant, etc.

Also, in this setup the operator can choose to use some of available IPs as "proxies" in order to avoid revealing teamserver's listener IP.

Wiki pages provide a detailed walk through of the setup process as well information about tests on the proposed infrastructure. Some OPSEC considerations are also noted in the testing wiki page. Nothing new that hasn't already been analyzed by the creator of cobaltstrike (Raphael Mudge). These considerations are mainly mentioned as a reminder.

The prerequisite for this setup is that you already own a licensed version of Cobaltstrike. Of course Cobaltstrike is not provided during this setup process.

