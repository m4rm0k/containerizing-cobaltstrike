#!/bin/bash

#########################
## @Created by nv2lt   ##
#########################

# Global variables
BLUE='\033[1;34m'
RED='\033[1;31m'
YELLOW='\033[0;33m'
OTHER='\033[1;41m'
NC='\033[0m'

PLATFORM=$(lsb_release -d | awk -F"\t" '{print $2}' | awk -F" " '{print $1}' )

COMBINED_IPS=""

USER_HOME=`getent passwd $(logname) |cut -f6 -d:`

DOCKERDIRECTORY="$(getent passwd $(logname) | cut -f6 -d:)/docker_playground"

if [ "$EUID" -ne 0 ]
  then echo -e "${RED} Please run with sudo or root${NC}"
  exit
fi


# Check if Docker-ce is already installed
echo -e "${BLUE}Installing docker-ce${NC}"
# Check if dnsmasq is already installed
INSTALLED=$(dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed")
if [ ${INSTALLED} == "0" ]
    then 
        if [ ${PLATFORM} == "Debian" ] || [ ${PLATFORM} == "Kali" ]
        then
            apt-get remove docker docker-engine docker.io
	    apt-get update
	    apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
            curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
            add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
	    apt-get update
            apt-get install -y docker-ce

        else 
            echo -e "${RED} This is not a Debian or KALI installation. Install docker-ce package manually. Exiting${NC}"
            exit
        fi
    else
        echo -e "${YELLOW}Docker-ce is already installed ${NC}"
        service docker restart
fi

# Create user and group docker if it does not exist
if grep -q docker /etc/group
then
    echo -e "${YELLOW}group docker was already created ${NC}"
    if id -nGz "$(logname)" | grep -qzxF "docker"
    then
	      echo -e "${YELLOW}User $(logname) belongs to group docker ${NC}"
    else
	      usermod -aG docker $(logname)
	      echo -e "${BLUE}User $(logname) is added to group docker ${NC}"
    fi
else
    echo -e "${BLUE}Group docker does not exist. It will be created and $USER will be added to it, so that $USER can run docker commands without typing sudo first ${NC}"
    groupadd docker
    usermod -aG docker $(logname)
fi


echo -e "${BLUE}Downloading alpine linux with openjdk11 slim image${NC}"
docker pull adoptopenjdk/openjdk11-openj9:x86_64-alpine-jdk-11.0.3_7_openj9-0.14.3-slim

sleep 1

echo -e "${BLUE}Download Completed. Time to create containers${NC}"
echo ""

sleep 2

create_container () {
    t1=$1 # ipv4(6)
    t2=$2 # http(s)
    t3=$3 # Directory in HOST Machine that Container will mount in /opt/cobaltstrike
    t4=$4 # Server Number
    MANAG_IP=""
    MANAG_PORT=""
    BIND_IPS=""
    BIND_PORT=""
    COMBINED_IPS=""
    SERVER_NAME="${t4}-cobalt-${t1}-${t2}"

    clear
    echo "Container with the name ${SERVER_NAME} will be created."
    echo ""
    echo "Available $t1 IPs on host machine are: "
    if [ $t1 == "ipv4" ]
    then
        ip a |grep -v inet6 |grep -v docker| grep inet | cut -d " " -f 6 | cut -d "/" -f 1 |grep -v 127.0.0.1 
    elif [ $t1 == "ipv6" ]
    then
        IPS_AVAILABLE="ip a |grep -v docker| grep inet6 |cut -d ' ' -f6|grep -v 128| cut -d '/' -f1 | grep -v fe80"
        if [[ $(eval $IPS_AVAILABLE) = "" ]]
        then
            echo ""
            echo -e "${RED}No Global IPv6 available to bind. Exiting in 3 sec ${NC}"
            sleep 3
            exit 1
        else
            eval $IPS_AVAILABLE
        fi
    fi
    echo ""
    
    ACCEPT='n'
    until [ "${ACCEPT}" == 'y' ]
    do
        read -p "Choose management IP for ${SERVER_NAME}: " MANAG_IP
        read -p "Choose management port for ${SERVER_NAME} (default-> 50050): " MANAG_PORT
        MANAG_PORT=${MANAG_PORT:-50050}
    
        read -p "Enter IPs (space separated) on host machine that will be bound to ${SERVER_NAME}: " BIND_IPS
        echo -e "${BLUE}You have chosen $MANAG_IP on $MANAG_PORT for management and the following IPs to be bound to $t2 listener: ${NC}"
        for i in ${BIND_IPS[@]}
        do
            echo -e "${BLUE}HTTP Bind IP: ${NC}"$i
        done
        read -p 'Is this Correct? (y/n)?' ACCEPT
        
        until [ "${ACCEPT}" == 'y' ]  || [ "${ACCEPT}" == 'n' ]
        do
            read -p 'Please answer with y or n letter or quit: ' ACCEPT
        done
    done
    
    ## Create Binding IPs and Ports on Host Machine
    if [ $t2 == "http" ]
    then
        read -p "Container will bind internal port 80 to HOST Machine. Choose $t2 BIND port on HOST Machine for ${SERVER_NAME} (default-> 80): " BIND_PORT
        BIND_PORT=${BIND_PORT:-80}
        for i in ${BIND_IPS[@]}
        do
            COMBINED_IPS="$COMBINED_IPS -p ${i}:$BIND_PORT:80"
        done
    elif [ $t2 == "https" ]
    then
        read -p "Container will bind internal port 443 to HOST Machine .Choose host $t2 BIND port for ${SERVER_NAME} (default-> 443): " BIND_PORT
        BIND_PORT=${BIND_PORT:-443}
        for i in ${BIND_IPS[@]}
        do
            COMBINED_IPS="$COMBINED_IPS -p ${i}:$BIND_PORT:443"
        done
    fi

    #echo $COMBINED_IPS
#    STRINGSTEST="docker run --name='$SERVER_NAME' -v $t3:/opt/cobaltstrike -p $MANAG_IP:$MANAG_PORT:50050 $COMBINED_IPS -dti  adoptopenjdk/openjdk11-openj9:x86_64-alpine-jdk-11.0.3_7_openj9-0.14.3-slim sh"
    echo ""
#    echo "Container ${SERVER_NAME} will be created with the following command: "
#    echo $STRINGSTEST
#    read -p 'Proceed? (y)?' ACCEPT
#    until [ "${ACCEPT}" == 'y' ]
#    do
#        read -p 'Please answer with y letter or quit: ' ACCEPT
#    done
            
	if [ ! -d $t3 ]
    then
		echo -e "${BLUE}Directory $t3 will be created for this container. ${NC}"
        mkdir -p $t3
    fi

	  if [ ! -f "$t3"/teamserver ]
	  then
        read -p "Enter full path of the directory of cobalstrike in order to copy it to ${t3} (default-> ${USER_HOME}/cobaltstrike/: " COBALTSTRIKEPATH
        COBALTSTRIKEPATH=${COBALTSTRIKEPATH:-"${USER_HOME}/cobaltstrike/"}
        TRIMMED_COBALTSTRIKEPATH=$(echo $COBALTSTRIKEPATH | sed 's:/*$::')
        until [ -f "$TRIMMED_COBALTSTRIKEPATH"/teamserver ]
        do
            read -p "$TRIMMED_COBALTSTRIKEPATH Directory doesn't contain cobaltrstike's files. Please re-enter path: " COBALTSTRIKEPATH
        done
        cp -r "$TRIMMED_COBALTSTRIKEPATH"/* "$t3"/
		chown -R $(logname):$(logname) $DOCKERDIRECTORY
	  fi
 
    #Create the Container
    #eval $STRINGTEST
    #docker run -d --name='cobalt-ipv4-http' -v "${t3}":/opt/cobaltstrike -p "${MANAG_IP}:${MANAG_PORT}":50050 "${COMBINED_IPS}" -ti  adoptopenjdk/openjdk11-openj9:x86_64-alpine-jdk-11.0.3_7_openj9-0.14.3-slim sh
    #docker run -d --name='cobalt-ipv4-http' -v $DIR_HTTP_IPV4:/opt/cobaltstrike -p $MANAG_IP:$MANAG_PORT:50050 $COMBINED_IPS -ti  adoptopenjdk/openjdk11-openj9:x86_64-alpine-jdk-11.0.3_7_openj9-0.14.3-slim sh
    docker run -d --name="$SERVER_NAME" -v $t3:/opt/cobaltstrike -p $MANAG_IP:$MANAG_PORT:50050 $COMBINED_IPS -ti  adoptopenjdk/openjdk11-openj9:x86_64-alpine-jdk-11.0.3_7_openj9-0.14.3-slim sh


    docker exec ${SERVER_NAME} apk add bash
    docker exec ${SERVER_NAME} apk add screen

    sleep 3
}


NO=1
while true
do
    read -p "Please enter TeamSrv Number you want to build (e.g. 1,2,3,... default-> ${NO} - type q to exit): " SRV_NO
    SRV_NO=${SRV_NO:-${NO}}
    if [ $SRV_NO == 'q' ]
    then
        exit 1
    fi
    while ! [[ "$SRV_NO" =~ ^[0-9]+$ ]]
    do
        read -p "This is not a number. Please enter a number (e.g. 1,2,3,... default-> ${NO}: " SRV_NO
        SRV_NO=${SRV_NO:-${NO}}
    done
    

    DIR_HTTP_IPV4="$DOCKERDIRECTORY/cobalt-srv-$SRV_NO-http_ipv4"
    DIR_HTTPS_IPV4="$DOCKERDIRECTORY/cobalt-srv-$SRV_NO-https_ipv4"
    DIR_HTTP_IPV6="$DOCKERDIRECTORY/cobalt-srv-$SRV_NO-http_ipv6"
    DIR_HTTPS_IPV6="$DOCKERDIRECTORY/cobalt-srv-$SRV_NO-https_ipv6"
    
    PS3='Which type of teamserver do you want to build? (1-4): '
    options=("Cobalt Container for IPv4-HTTP" "Cobalt Container for IPv4-HTTPs"  "Cobalt Container for IPv6-HTTP" "Cobalt Container for IPv6-HTTPs" "Quit")

    clear
    echo -e "${BLUE}You can build the following containers: ${NC}"
    select opt in "${options[@]}"
    do
        case $REPLY in
            1)
                ###### Create IPv4-HTTP Container
                create_container "ipv4" "http" "$DIR_HTTP_IPV4" "$SRV_NO"
                break        
                ;;
        
            2)
                ###### Create IPv4-HTTP Container
                create_container "ipv4" "https" "$DIR_HTTPS_IPV4" "$SRV_NO"
                break
                ;;
        
                ###### Create IPv6-HTTP Container 
            3)
                create_container "ipv6" "http" "$DIR_HTTP_IPV6" "$SRV_NO"
                break
                ;;
         
                ####### Create IPv6-HTTPS Container
            4)
                create_container "ipv6" "https" "$DIR_HTTPS_IPV6" "$SRV_NO"
                break
                ;;
        
                ####### "Quit")
            5)
                break 2
                ;;
        
            *) echo "invalid option $REPLY";;
          esac
    done
    NO=$((NO + 1))
done

